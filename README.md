This repository is archived and is supersuperseded by an updated version of our algorithm which can be found at [here](https://bitbucket.org/vdasu_edu/boyar-peralta-xor3/).

# boyar-peralta-xor3

The repository the contains implementation of the paper [**Another Look at Boyar-Peralta�s Algorithm**](https://www.springer.com/gp/book/9783030816445) by [Anubhab Baksi](mailto:anubhab001@e.ntu.edu.sg),[Banashri Karmakar](mailto:banashrik@iitbhilai.ac.in) and [Vishnu Asutosh Dasu](mailto:vishnu.dasu1@tcs.com).

## Usage

To compile the program, the following options need to be passed:

    TIME_LIMIT - The amount of time (in seconds) allowed to run the program (default 1000)
    XOR2C - Cost of implementing XOR2 gate (default 1.0)
    XOR3C - Cost of implementing XOR3 gate (default 1.625)

Sample command:

    g++ -std=c++11 -o main_globalopt.out -D XOR2C=1.0 -D XOR3C=1.625 -D TIME_LIMIT=1000 main_globalopt.cpp

Pass the matrix to the generated executable to run the program:

    ./main_globalopt.out < matrix.txt

### Input format

The text file contains the number of rows and columns in the matrix followed by the matrix.

For example:
```
    15 15
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
    0 0 1 0 0 0 0 0 0 0 0 0 0 0 0
    0 0 0 1 0 0 0 0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 1 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0 0 1 0 0 0 0 0
    1 0 1 0 0 0 0 0 0 0 0 0 0 0 0
    0 0 0 0 0 1 0 0 0 0 0 1 0 0 0
    0 0 0 0 0 0 1 0 0 0 1 0 1 0 0
    0 0 0 0 0 0 0 0 0 1 1 0 1 0 0
    1 0 0 0 0 0 0 0 0 0 0 1 0 0 0
    0 0 0 0 0 1 0 0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0 0 0 0 0 1 0 1
    0 0 0 0 1 0 0 0 0 0 0 0 1 0 0
    0 0 0 0 1 0 0 0 1 0 0 0 1 0 0
    0 0 0 0 0 1 0 1 0 0 0 0 0 0 0
```